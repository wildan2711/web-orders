GO := $(shell command -v go 2> /dev/null)
PNPM := $(shell command -v pnpm 2> /dev/null)

setup:
ifndef GO
	$(error "go is not installed")
endif
ifndef PNPM
	$(error "pnpm is not installed")
endif
	@cp -n config.yaml.example config.yaml
	@cd client && pnpm install

build:
	@go build
	@cd client && pnpm run build

run:
	@./web-orders