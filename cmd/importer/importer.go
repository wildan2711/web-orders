package importer

import (
	"log"
	"os"
	"sync"

	"github.com/spf13/cobra"
	"gitlab.com/wildan2711/web-orders/config"
	"gitlab.com/wildan2711/web-orders/tools/importer"
)

func run(importKey string, files []string, withHeader bool) {
	conf, err := config.Load()
	if err != nil {
		log.Fatalln("error loading config", err)
	}

	importerFactory := &importer.ImporterFactory{Cfg: conf}
	defer importerFactory.Close()

	var wg sync.WaitGroup
	wg.Add(len(files))

	for _, filePath := range files {
		go func(path string) {
			defer wg.Done()

			log.Printf(`importing "%s" as %s`, path, importKey)

			file, err := os.Open(path)
			if err != nil {
				log.Println("error opening file:", err)
				return
			}

			importerInstance, err := importerFactory.GetImporter(importKey)
			if err != nil {
				log.Println(err)
				return
			}

			importer.Import(file, importerInstance, withHeader)

			log.Printf(`completed import "%s" as %s`, path, importKey)
		}(filePath)
	}

	wg.Wait()
}

func Command() *cobra.Command {
	withHeader := true

	cmd := &cobra.Command{
		Use: "import [import key] [files...]",
		Run: func(cmd *cobra.Command, args []string) {
			importKey := args[0]
			files := args[1:]

			run(importKey, files, withHeader)
		},
	}
	cmd.Flags().BoolVar(&withHeader, "with-header", true, "specified whether the input file includes a header row")

	return cmd
}
