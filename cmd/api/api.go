package api

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v9"
	"github.com/spf13/cobra"
	"gitlab.com/wildan2711/web-orders/config"
	"gitlab.com/wildan2711/web-orders/lib/db/mongodb"
	"gitlab.com/wildan2711/web-orders/lib/db/pgdb"
	"gitlab.com/wildan2711/web-orders/repository/customer"
	"gitlab.com/wildan2711/web-orders/repository/order"
)

type dbLogger struct{}

func (d dbLogger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

func (d dbLogger) AfterQuery(c context.Context, q *pg.QueryEvent) error {
	query, _ := q.FormattedQuery()
	fmt.Println("[GO-PG][DEBUG]", query)
	return nil
}

func run() {
	cfg, err := config.Load()
	if err != nil {
		log.Fatalln("error reading config:", err)
	}

	pg, err := pgdb.FromConfig(cfg)
	if err != nil {
		log.Fatalln("error pg from config:", err)
	}
	if cfg.Connection.Postgres.Debug {
		pg.AddQueryHook(dbLogger{})
	}

	mongodb, err := mongodb.FromConfig(cfg)
	if err != nil {
		log.Fatalln("error mongo from config:", err)
	}

	ctrler := &controller{
		OrderRepository:    order.NewPgRepository(pg),
		CustomerRepository: customer.NewMongoRepository(mongodb),
	}

	router := gin.Default()

	// Static server to serve SPA frontend
	router.Use(static.Serve("/", static.LocalFile(cfg.Server.StaticDir, false)))
	router.NoRoute(func(c *gin.Context) {
		c.File(cfg.Server.StaticDir + "/index.html")
	})

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
	}))
	router.GET("/api/v1/orders", ctrler.OrderList())

	server := &http.Server{
		Addr:    cfg.Server.Address,
		Handler: router,
	}

	log.Printf("HTTP API server running at %s", server.Addr)

	go func() {
		// service connections
		if err := server.ListenAndServe(); err != nil {
			log.Printf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)

	signal.Notify(quit, os.Interrupt)
	<-quit

	log.Println("Shutting down Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalln("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}

func Command() *cobra.Command {
	return &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) {
			run()
		},
	}
}
