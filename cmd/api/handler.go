package api

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/wildan2711/web-orders/lib/types/set"
	"gitlab.com/wildan2711/web-orders/models"
	"gitlab.com/wildan2711/web-orders/repository/customer"
	"gitlab.com/wildan2711/web-orders/repository/order"
)

type controller struct {
	CustomerRepository customer.Repository
	OrderRepository    order.Repository
}

func (ctrler *controller) retrieveOrders(ctx context.Context, params *models.OrderListParams) (*models.OrderListResults, error) {
	result, err := ctrler.OrderRepository.List(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("error order list: %w", err)
	}

	customerIds := set.NewSet()
	for _, _order := range result.Orders {
		customerIds.Add(_order.CustomerID)
	}

	customerMap, err := ctrler.CustomerRepository.GetMany(ctx, customerIds.ToSlice()...)
	if err != nil {
		return nil, fmt.Errorf("error get customer: %w", err)
	}

	for _, _order := range result.Orders {
		_order.Customer = customerMap[_order.CustomerID]

		// calculate amount per order
		var deliveredAmount, totalAmount float64
		for _, item := range _order.Items {
			totalAmount += item.Price * float64(item.Quantity)
			for _, delivered := range item.Deliveries {
				deliveredAmount += item.Price * float64(delivered.Quantity)
			}
		}

		_order.DeliveredAmount = deliveredAmount
		_order.TotalAmount = totalAmount
	}
	return result, nil
}

func (ctrler *controller) OrderList() gin.HandlerFunc {
	return func(c *gin.Context) {
		var params models.OrderListParams

		err := c.BindQuery(&params)
		if err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		params.Defaults()

		ctx := c.Request.Context()

		result, err := ctrler.retrieveOrders(ctx, &params)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.JSON(http.StatusOK, result)
	}
}
