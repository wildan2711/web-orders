package api

import (
	"encoding/json"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/wildan2711/web-orders/models"
	mock_customer "gitlab.com/wildan2711/web-orders/repository/customer/mocks"
	mock_order "gitlab.com/wildan2711/web-orders/repository/order/mocks"
)

func Test_controller_OrderList(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	params := &models.OrderListParams{
		Keyword: "wildan",
		Limit:   5,
	}
	orderList := &models.OrderListResults{
		Orders: []*models.Order{
			{
				ID: 1,
				Items: []*models.OrderItem{
					{ID: 1, Price: 100, Quantity: 10, Deliveries: []*models.Delivery{
						{ID: 1, Quantity: 5},
					}},
					{ID: 2, Price: 50, Quantity: 20},
				},
				CustomerID: "wildan",
			},
		},
	}
	customerMap := map[string]*models.Customer{
		"wildan": {
			ID:    "wildan",
			Name:  "wildan",
			Login: "wildan",
		},
	}

	orderRepo := mock_order.NewMockRepository(ctrl)
	customerRepo := mock_customer.NewMockRepository(ctrl)

	orderRepo.EXPECT().List(gomock.Any(), params).Return(orderList, nil)
	customerRepo.EXPECT().GetMany(gomock.Any(), "wildan").Return(customerMap, nil)

	ctrler := controller{
		OrderRepository:    orderRepo,
		CustomerRepository: customerRepo,
	}

	handler := ctrler.OrderList()

	recorder := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(recorder)

	req := httptest.NewRequest("GET", "http://wildan.id?keyword=wildan", nil)
	c.Request = req

	handler(c)

	expected, err := json.Marshal(orderList)
	require.NoError(t, err)

	assert.Equal(t, expected, recorder.Body.Bytes())
}
