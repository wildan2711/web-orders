# Web Order App

Web application for listing orders.

## Requirements

- [Golang v1.13+](http://golang.org/)
- [PostgreSQL 9+](https://www.postgresql.org/download/)
- [MongoDB 3.2+](https://docs.mongodb.com/manual/administration/install-community/)
- [Node 10.15+](https://nodejs.org/en/)
- [PNPM 5+](https://pnpm.js.org/en/installation)

## Getting Started

1. Make sure you have a running PostgreSQL and MongoDB server.
2. Clone this repository.
```
git@gitlab.com:wildan2711/web-orders.git

cd web-orders
```

3. Run `make setup` to setup your environment.
4. Modify the generated `config.yaml` to suit your environment (PostgreSQL, MongoDB connections).
5. Run `make build` to build the app.

### Importer

Command line tool to import data.

Usage:

```
./web-orders import <import_key> <file paths...> # can pass multiple files
```

Example:

```
./web-orders import order "Test task - Postgres - orders.csv" "Test task - Postgres - orders2.csv" "Test task - Postgres - orders3.csv"
```

Available import keys:
* `order`
* `order_item`
* `delivery`
* `customer`
* `company`

### Web application

Web application to display the data.

Run this command to run the server:
```
./web-orders
```

Open `http://localhost:8080/orders` in your browser (modify the address if you specified a different server address in your `config.yaml`).

## Development

### Frontend

Run the development client file watcher.

```
cd client

pnpm run dev
```

Refresh the browser on any file changes.

## Unit Testing

### Backend

Run the test command.

```
go test ./... -cover
```

### Frontend

```
cd client

pnpm run test:unit
```