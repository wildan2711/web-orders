package customer

//go:generate mockgen -destination=./mocks/mock_repository.go gitlab.com/wildan2711/web-orders/repository/customer Repository

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/wildan2711/web-orders/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

type Repository interface {
	GetMany(ctx context.Context, ids ...string) (map[string]*models.Customer, error)

	Insert(context.Context, *models.Customer) error
	InsertMany(context.Context, []*models.Customer) error
}

type mongoRepository struct {
	collection *mongo.Collection
}

func (r *mongoRepository) GetMany(ctx context.Context, ids ...string) (map[string]*models.Customer, error) {
	matchStage := bson.D{{"$match", bson.D{{"id", bson.D{{"$in", ids}}}}}}
	lookupStage := bson.D{{"$lookup", bson.D{{"from", "company"}, {"localField", "company_id"}, {"foreignField", "id"}, {"as", "company"}}}}
	unwindStage := bson.D{{"$unwind", bson.D{{"path", "$company"}, {"preserveNullAndEmptyArrays", false}}}}

	cursor, err := r.collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage})
	if err != nil {
		return nil, fmt.Errorf("error mongodb aggregate: %w", err)
	}

	var customers []*models.Customer
	if err = cursor.All(ctx, &customers); err != nil {
		return nil, fmt.Errorf("error cursor all: %w", err)
	}

	customerMap := make(map[string]*models.Customer)
	for _, customer := range customers {
		customerMap[customer.ID] = customer
	}

	return customerMap, nil
}

func (r *mongoRepository) Insert(ctx context.Context, customer *models.Customer) error {
	// hash the password
	hashed, err := bcrypt.GenerateFromPassword([]byte(customer.Password), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("error bcrypt hash: %w", err)
	}
	customer.Password = string(hashed)

	_, err = r.collection.InsertOne(ctx, customer)
	if err != nil {
		return fmt.Errorf("error InsertOne: %w", err)
	}

	return nil
}

func (r *mongoRepository) InsertMany(ctx context.Context, customers []*models.Customer) error {
	interfaceCustomers := make([]interface{}, len(customers))
	for idx, customer := range customers {
		// hash the password
		hashed, err := bcrypt.GenerateFromPassword([]byte(customer.Password), bcrypt.DefaultCost)
		if err != nil {
			return fmt.Errorf("error bcrypt hash: %w", err)
		}
		customer.Password = string(hashed)
		interfaceCustomers[idx] = customer
	}

	_, err := r.collection.InsertMany(ctx, interfaceCustomers)
	if err != nil {
		return fmt.Errorf("error InsertMany: %w", err)
	}

	return nil
}

func ensureIndices(collection *mongo.Collection) {
	idxs := collection.Indexes()
	_, err := idxs.CreateMany(context.Background(), []mongo.IndexModel{
		{
			Keys:    bson.D{{"id", 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{"login", 1}},
			Options: options.Index().SetUnique(true),
		},
	})
	if err != nil {
		log.Println("error creating indices", err)
	}
}

// NewMongoRepository creates a Customer repository using mongodb
func NewMongoRepository(db *mongo.Database) Repository {
	collection := db.Collection("customer")
	ensureIndices(collection)
	return &mongoRepository{collection: collection}
}
