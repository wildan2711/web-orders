package order

//go:generate mockgen -destination=./mocks/mock_repository.go gitlab.com/wildan2711/web-orders/repository/order Repository

import (
	"context"
	"fmt"
	"log"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"gitlab.com/wildan2711/web-orders/models"
	"golang.org/x/sync/errgroup"
)

type Repository interface {
	Insert(context.Context, *models.Order) error
	InsertItem(context.Context, *models.OrderItem) error
	InsertDelivery(context.Context, *models.Delivery) error

	List(context.Context, *models.OrderListParams) (*models.OrderListResults, error)
}

type pgRepository struct {
	db *pg.DB
}

func (r *pgRepository) Insert(ctx context.Context, order *models.Order) error {
	_, err := r.db.WithContext(ctx).
		Model(order).
		OnConflict("(id) DO UPDATE").
		Set("order_name = EXCLUDED.order_name").
		Set("customer_id = EXCLUDED.customer_id").
		Set("created_at = EXCLUDED.created_at").
		Insert()
	if err != nil {
		return fmt.Errorf("error pg insert: %w", err)
	}
	return nil
}

func (r *pgRepository) InsertItem(ctx context.Context, item *models.OrderItem) error {
	_, err := r.db.WithContext(ctx).
		Model(item).
		OnConflict("(id) DO UPDATE").
		Set("order_id = EXCLUDED.order_id").
		Set("price = EXCLUDED.price").
		Set("quantity = EXCLUDED.quantity").
		Set("product = EXCLUDED.product").
		Insert()
	if err != nil {
		return fmt.Errorf("error pg insert: %w", err)
	}
	return nil
}

func (r *pgRepository) InsertDelivery(ctx context.Context, delivery *models.Delivery) error {
	_, err := r.db.WithContext(ctx).
		Model(delivery).
		OnConflict("(id) DO UPDATE").
		Set("order_item_id = EXCLUDED.order_item_id").
		Set("quantity = EXCLUDED.quantity").
		Insert()
	if err != nil {
		return fmt.Errorf("error pg insert: %w", err)
	}
	return nil
}

func applyFilters(query *orm.Query, params *models.OrderListParams) {
	if params.Keyword != "" {
		query.
			Where(`"order".order_name ILIKE '%' || ? || '%'`, params.Keyword).
			WhereOr("oi.product ILIKE  '%' || ? || '%'", params.Keyword)
	}

	if !params.From.IsZero() && !params.To.IsZero() {
		query.Where(`"order".created_at BETWEEN ? AND ?`, params.From, params.To)
	} else if !params.From.IsZero() {
		query.Where(`"order".created_at >= ?`, params.From)
	} else if !params.To.IsZero() {
		query.Where(`"order".created_at <= ?`, params.To)
	}
}

func (r *pgRepository) List(ctx context.Context, params *models.OrderListParams) (*models.OrderListResults, error) {
	var orders []*models.Order

	wg, ctx := errgroup.WithContext(ctx)
	wg.Go(func() error {
		listQuery := r.db.WithContext(ctx).
			Model(&orders).
			Column("order.*").
			Join(`LEFT JOIN order_items oi ON oi.order_id = "order".id`).
			Relation("Items").
			Relation("Items.Deliveries")

		applyFilters(listQuery, params)

		err := listQuery.
			Limit(params.Limit).
			Offset(params.Skip).
			Select()
		if err != nil {
			return fmt.Errorf("error query order list: %w", err)
		}
		return nil
	})

	summary := &models.OrderSummary{}
	wg.Go(func() error {
		summaryQuery := r.db.WithContext(ctx).
			Model(summary).
			ColumnExpr(`sum(oi.price * oi.quantity) AS total_amount, count("order".id) AS total_count`).
			Join(`LEFT JOIN order_items oi ON oi.order_id = "order".id`)

		applyFilters(summaryQuery, params)

		err := summaryQuery.Select()
		if err != nil {
			return fmt.Errorf("error query order summary: %w", err)
		}
		return nil
	})

	if err := wg.Wait(); err != nil {
		return nil, err
	}

	return &models.OrderListResults{
		Orders:  orders,
		Summary: summary,
	}, nil
}

// NewPgRepository returns a new Order repository using go-pg
func NewPgRepository(db *pg.DB) Repository {
	models := []interface{}{
		(*models.Order)(nil),
		(*models.OrderItem)(nil),
		(*models.Delivery)(nil),
	}
	for _, model := range models {
		if err := db.CreateTable(model, &orm.CreateTableOptions{IfNotExists: true, FKConstraints: true}); err != nil {
			log.Println("error creating table:", err)
		}
	}
	db.Exec("CREATE INDEX IF NOT EXISTS orders_name_idx ON orders USING btree (lower(order_name))")
	db.Exec("CREATE INDEX IF NOT EXISTS order_created_at_idx ON orders USING btree (created_at)")
	db.Exec("CREATE INDEX IF NOT EXISTS order_items_product_idx ON order_items USING btree (lower(product))")
	return &pgRepository{db}
}
