package company

//go:generate mockgen -destination=./mocks/mock_repository.go gitlab.com/wildan2711/web-orders/repository/company Repository

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/wildan2711/web-orders/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repository interface {
	Insert(context.Context, *models.Company) error
	InsertMany(context.Context, []*models.Company) error
}

type mongoRepository struct {
	collection *mongo.Collection
}

func (r *mongoRepository) Insert(ctx context.Context, company *models.Company) error {
	_, err := r.collection.InsertOne(ctx, company)
	if err != nil {
		return fmt.Errorf("error InsertOne: %w", err)
	}

	return nil
}

func (r *mongoRepository) InsertMany(ctx context.Context, companies []*models.Company) error {
	interfaceCompanies := make([]interface{}, len(companies))
	for idx, company := range interfaceCompanies {
		interfaceCompanies[idx] = company
	}

	_, err := r.collection.InsertMany(ctx, interfaceCompanies)
	if err != nil {
		return fmt.Errorf("error InsertOne: %w", err)
	}

	return nil
}

func ensureIndices(collection *mongo.Collection) {
	idxs := collection.Indexes()
	_, err := idxs.CreateMany(context.Background(), []mongo.IndexModel{
		{
			Keys:    bson.D{{"id", 1}},
			Options: options.Index().SetUnique(true),
		},
	})
	if err != nil {
		log.Println("error creating indices", err)
	}
}

// NewMongoRepository creates a Company repository using mongodb
func NewMongoRepository(db *mongo.Database) Repository {
	collection := db.Collection("company")
	ensureIndices(collection)
	return &mongoRepository{collection: collection}
}
