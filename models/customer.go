package models

type Customer struct {
	ID          string   `json:"id" bson:"id"`
	Login       string   `json:"login" bson:"login"`
	Password    string   `json:"-" bson:"password"`
	Name        string   `json:"name" bson:"name"`
	CompanyID   int      `json:"companyId" bson:"companyId"`
	Company     *Company `json:"company,omitempty" bson:"company"`
	CreditCards []string `json:"creditCards" bson:"creditCards"`
}
