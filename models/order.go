package models

import "time"

type Order struct {
	ID              int          `json:"id" pg:"type:bigserial"`
	CustomerID      string       `json:"customerId"`
	Customer        *Customer    `json:"customer,omitempty" pg:"-"`
	OrderName       string       `json:"orderName"`
	Items           []*OrderItem `json:"items"`
	DeliveredAmount float64      `json:"deliveredAmount" pg:"-"`
	TotalAmount     float64      `json:"totalAmount" pg:"-"`
	CreatedAt       time.Time    `json:"createdAt" pg:"default:now()"`
}

type OrderItem struct {
	ID         int         `json:"id" pg:"type:bigserial"`
	OrderID    int         `json:"orderId"`
	Price      float64     `json:"price" pg:"type:decimal(15,4)"`
	Quantity   int         `json:"quantity"`
	Product    string      `json:"product"`
	Order      *Order      `json:"-" pg:"fk:order_id"` // for go-pg fk purpose
	Deliveries []*Delivery `json:"deliveries"`
}

type Delivery struct {
	ID          int        `json:"id" pg:"type:bigserial"`
	OrderItemID int        `json:"orderItemId"`
	OrderItem   *OrderItem `json:"-" pg:"fk:order_item_id"` // for go-pg fk purpose
	Quantity    int        `json:"quantity"`
}

type OrderListParams struct {
	Limit    int    `form:"limit"`
	Skip     int    `form:"skip"`
	Keyword  string `form:"keyword"`
	DateFrom string `form:"date_from"`
	DateTo   string `form:"date_to"`
	SortAsc  string `form:"sort_asc"`
	SortDesc string `form:"sort_desc"`
	From     time.Time
	To       time.Time
}

// Defaults will set default values
func (p *OrderListParams) Defaults() {
	if p.Limit == 0 {
		p.Limit = 5
	}

	if p.DateFrom != "" {
		from, err := time.Parse(time.RFC3339, p.DateFrom)
		if err == nil {
			p.From = from
		}
	}

	if p.DateTo != "" {
		to, err := time.Parse(time.RFC3339, p.DateTo)
		if err == nil {
			p.To = to
		}
	}
}

type OrderSummary struct {
	tableName   struct{} `pg:"orders,alias:order"`
	TotalAmount float64  `json:"totalAmount"`
	TotalCount  int      `json:"totalCount"`
}

type OrderListResults struct {
	Orders  []*Order      `json:"orders"`
	Summary *OrderSummary `json:"summary"`
}
