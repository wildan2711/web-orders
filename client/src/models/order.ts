export interface OrderResponse {
  orders: Order[] | null
  summary: OrderSummary
}

export interface OrderSummary {
  totalAmount: number
  totalCount: number
}

export interface Order {
  id: number
  customerId: string
  customer: Customer
  orderName: string
  items: Item[]
  deliveredAmount: number
  totalAmount: number
  deliveredAmountStr?: string
  totalAmountStr?: string
  createdAt: string
}

export interface Item {
  id: number
  orderId: number
  price: number
  quantity: number
  product: string
  deliveries?: (Delivery[] | null)[]
}

export interface Delivery {
  id: number
  orderItemId: number
  quantity: number
}

export interface Customer {
  id: string
  login: string
  name: string
  companyId: number
  company: Company
  creditCards?: string[] | null
}

export interface Company {
  id: number
  name: string
}
