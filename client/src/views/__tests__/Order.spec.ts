import { mount, createLocalVue } from "@vue/test-utils"
import Vuetify from "vuetify"
import Orders from "../Orders.vue"
import mockData from "../__data_mocks__/orderList"

jest.mock("ky", () => ({
  get: () => ({
    json: async () => {
      return mockData
    }
  })
}))

const localVue = createLocalVue()

describe("@/views/Orders.vue", () => {
  let vuetify: any

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  test("renders the fetched data on the data table", async () => {
    const wrapper = mount(Orders, {
      localVue,
      vuetify
    })
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(wrapper.findAll("tbody > tr")).toHaveLength(5)
  })
})
