export default {
  orders: [
    {
      id: 1,
      customerId: "ivan",
      customer: {
        id: "ivan",
        login: "ivan",
        name: "Ivan Ivanovich",
        companyId: 0,
        company: { id: 1, name: "Roga \u0026 Kopyta" },
        creditCards: null
      },
      orderName: "PO #001-I",
      items: [
        {
          id: 1,
          orderId: 1,
          price: 1.3454,
          quantity: 10,
          product: "Corrugated Box",
          deliveries: [{ id: 1, orderItemId: 1, quantity: 5 }]
        },
        {
          id: 11,
          orderId: 1,
          price: 45.2334,
          quantity: 20,
          product: "Hand sanitizer",
          deliveries: null
        }
      ],
      deliveredAmount: 6.726999999999999,
      totalAmount: 918.1220000000001,
      createdAt: "2020-01-02T22:34:12+07:00"
    },
    {
      id: 2,
      customerId: "ivan",
      customer: {
        id: "ivan",
        login: "ivan",
        name: "Ivan Ivanovich",
        companyId: 0,
        company: { id: 1, name: "Roga \u0026 Kopyta" },
        creditCards: null
      },
      orderName: "PO #002-I",
      items: [
        {
          id: 2,
          orderId: 2,
          price: 23.14,
          quantity: 11,
          product: "Corrugated Box",
          deliveries: [{ id: 2, orderItemId: 2, quantity: 11 }]
        },
        {
          id: 12,
          orderId: 2,
          price: 0,
          quantity: 21,
          product: "Hand sanitizer",
          deliveries: null
        }
      ],
      deliveredAmount: 254.54000000000002,
      totalAmount: 254.54000000000002,
      createdAt: "2020-01-16T00:34:12+07:00"
    },
    {
      id: 3,
      customerId: "ivan",
      customer: {
        id: "ivan",
        login: "ivan",
        name: "Ivan Ivanovich",
        companyId: 0,
        company: { id: 1, name: "Roga \u0026 Kopyta" },
        creditCards: null
      },
      orderName: "PO #003-I",
      items: [
        {
          id: 3,
          orderId: 3,
          price: 123.0345,
          quantity: 12,
          product: "Corrugated Box",
          deliveries: [{ id: 3, orderItemId: 3, quantity: 12 }]
        },
        {
          id: 13,
          orderId: 3,
          price: 273.1234,
          quantity: 22,
          product: "Hand sanitiZER",
          deliveries: null
        }
      ],
      deliveredAmount: 1476.414,
      totalAmount: 7485.1287999999995,
      createdAt: "2020-01-05T12:34:12+07:00"
    },
    {
      id: 4,
      customerId: "ivan",
      customer: {
        id: "ivan",
        login: "ivan",
        name: "Ivan Ivanovich",
        companyId: 0,
        company: { id: 1, name: "Roga \u0026 Kopyta" },
        creditCards: null
      },
      orderName: "PO #004-I",
      items: [
        {
          id: 4,
          orderId: 4,
          price: 0,
          quantity: 13,
          product: "Corrugated Box",
          deliveries: [
            { id: 4, orderItemId: 4, quantity: 3 },
            { id: 13, orderItemId: 4, quantity: 5 }
          ]
        },
        {
          id: 14,
          orderId: 4,
          price: 11.45,
          quantity: 23,
          product: "Hand sanitizer",
          deliveries: null
        }
      ],
      deliveredAmount: 0,
      totalAmount: 263.34999999999997,
      createdAt: "2020-02-02T22:34:12+07:00"
    },
    {
      id: 5,
      customerId: "ivan",
      customer: {
        id: "ivan",
        login: "ivan",
        name: "Ivan Ivanovich",
        companyId: 0,
        company: { id: 1, name: "Roga \u0026 Kopyta" },
        creditCards: null
      },
      orderName: "PO #005-I",
      items: [
        {
          id: 5,
          orderId: 5,
          price: 100,
          quantity: 14,
          product: "Corrugated Box",
          deliveries: null
        },
        {
          id: 15,
          orderId: 5,
          price: 12.467,
          quantity: 24,
          product: "Hand sanitizer",
          deliveries: null
        }
      ],
      deliveredAmount: 0,
      totalAmount: 1699.208,
      createdAt: "2020-01-03T17:34:12+07:00"
    }
  ],
  summary: { totalAmount: 27935.4241, totalCount: 20 }
}
