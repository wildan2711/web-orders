module.exports = {
  moduleNameMapper: {
    "^ky$": require.resolve("ky").replace("index.js", "umd.js")
  },
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  setupFilesAfterEnv: ["./jest.setup.js"]
}
