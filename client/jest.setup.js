import Vue from "vue"
import Vuetify from "vuetify"

Vue.use(Vuetify)

Vue.config.productionTip = false

const el = document.createElement("div")
el.setAttribute("data-app", "true")
document.body.appendChild(el)
