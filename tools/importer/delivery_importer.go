package importer

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/wildan2711/web-orders/models"
	"gitlab.com/wildan2711/web-orders/repository/order"
)

const (
	ColDeliveryID = iota
	ColDeliveryOrderItemID
	ColDeliveryQuantity
)

type DeliveryImporter struct {
	repository order.Repository
}

func (i *DeliveryImporter) Insert(record []string) error {
	if len(record) != ColDeliveryQuantity+1 {
		return errors.New("invalid number of columns")
	}

	delivery := models.Delivery{}
	deliveryID, err := strconv.Atoi(record[ColDeliveryID])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}
	orderItemID, err := strconv.Atoi(record[ColDeliveryOrderItemID])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}
	quantity, err := strconv.Atoi(record[ColDeliveryQuantity])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}

	delivery.ID = deliveryID
	delivery.OrderItemID = orderItemID
	delivery.Quantity = quantity

	if err := i.repository.InsertDelivery(context.Background(), &delivery); err != nil {
		return fmt.Errorf("error repo insert: %w", err)
	}

	return nil
}
