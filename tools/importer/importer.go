package importer

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"log"

	"github.com/go-pg/pg/v9"
	"gitlab.com/wildan2711/web-orders/config"
	"gitlab.com/wildan2711/web-orders/lib/db/mongodb"
	"gitlab.com/wildan2711/web-orders/lib/db/pgdb"
	"gitlab.com/wildan2711/web-orders/repository/company"
	"gitlab.com/wildan2711/web-orders/repository/customer"
	"gitlab.com/wildan2711/web-orders/repository/order"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	ImportKeyCompany   = "company"
	ImportKeyCustomer  = "customer"
	ImportKeyOrder     = "order"
	ImportKeyOrderItem = "order_item"
	ImportKeyDelivery  = "delivery"
)

var (
	ImportKeys = []string{
		ImportKeyCompany,
		ImportKeyCustomer,
		ImportKeyOrder,
		ImportKeyOrderItem,
		ImportKeyDelivery,
	}
)

type Importer interface {
	Insert(record []string) error
}

func Import(csvFile io.Reader, importer Importer, withHeader bool) {
	r := csv.NewReader(csvFile)

	header := false
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}

		if withHeader && !header {
			header = true
			continue
		}

		if err != nil {
			log.Println("error reading csv record:", err)
		}

		if err := importer.Insert(record); err != nil {
			log.Println("error inserting record:", err)
		}
	}
}

type ImporterFactory struct {
	Cfg   *config.Config
	mongo *mongo.Database
	pg    *pg.DB
}

func (f *ImporterFactory) mongodb() error {
	db, err := mongodb.FromConfig(f.Cfg)
	if err != nil {
		return fmt.Errorf("error mongodb from config: %w", err)
	}
	f.mongo = db
	return nil
}

func (f *ImporterFactory) postgres() error {
	db, err := pgdb.FromConfig(f.Cfg)
	if err != nil {
		return fmt.Errorf("error pg from config: %w", err)
	}
	f.pg = db
	return nil
}

// GetImporter returns a new data importer based on the import key
func (f *ImporterFactory) GetImporter(importKey string) (importer Importer, err error) {
	switch importKey {
	case ImportKeyCustomer:
		err = f.mongodb()
		importer = &CustomerImporter{customer.NewMongoRepository(f.mongo)}
	case ImportKeyCompany:
		err = f.mongodb()
		importer = &CompanyImporter{company.NewMongoRepository(f.mongo)}
	case ImportKeyOrder, ImportKeyOrderItem, ImportKeyDelivery:
		err = f.postgres()
		repo := order.NewPgRepository(f.pg)
		switch importKey {
		case ImportKeyOrder:
			importer = &OrderImporter{repo}
		case ImportKeyOrderItem:
			importer = &OrderItemImporter{repo}
		case ImportKeyDelivery:
			importer = &DeliveryImporter{repo}
		}
	default:
		err = fmt.Errorf("import key %s not found", importKey)
	}

	if err != nil {
		return nil, fmt.Errorf("error getting %s importer: %w", importKey, err)
	}

	return
}

// Close any used connections
func (f *ImporterFactory) Close() error {
	if f.mongo != nil {
		if err := f.mongo.Client().Disconnect(context.Background()); err != nil {
			return fmt.Errorf("error disconnecting mongo: %w", err)
		}
	}

	if f.pg != nil {
		if err := f.pg.Close(); err != nil {
			return fmt.Errorf("error closing pg: %w", err)
		}
	}

	return nil
}
