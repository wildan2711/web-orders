package importer

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/wildan2711/web-orders/models"
	mock_order "gitlab.com/wildan2711/web-orders/repository/order/mocks"
)

func TestDeliveryImporter_Insert(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	delivery := &models.Delivery{
		ID:          1,
		OrderItemID: 1,
		Quantity:    3,
	}

	mockRepository := mock_order.NewMockRepository(ctrl)
	mockRepository.EXPECT().InsertDelivery(ctx, delivery).Return(nil)
	mockRepository.EXPECT().InsertDelivery(ctx, delivery).Return(errors.New("error my bad"))

	i := &DeliveryImporter{
		repository: mockRepository,
	}
	type args struct {
		record []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should successfully insert the delivery",
			args: args{
				record: []string{"1", "1", "3"},
			},
		},
		{
			name: "should return unexpected error on insert",
			args: args{
				record: []string{"1", "1", "3"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing delivery id",
			args: args{
				record: []string{"wildan", "1", "1"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing order item id",
			args: args{
				record: []string{"1", "wildan", "1"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing quantity",
			args: args{
				record: []string{"1", "1", "wildan"},
			},
			wantErr: true,
		},
		{
			name: "should return error on invalid number of columns",
			args: args{
				record: []string{"wildan"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := i.Insert(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("DeliveryImporter.Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
