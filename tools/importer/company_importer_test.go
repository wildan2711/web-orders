package importer

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/wildan2711/web-orders/models"
	mock_company "gitlab.com/wildan2711/web-orders/repository/company/mocks"
)

func TestCompanyImporter_Insert(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	company := &models.Company{
		ID:   1,
		Name: "wildan",
	}

	mockRepository := mock_company.NewMockRepository(ctrl)
	mockRepository.EXPECT().Insert(ctx, company).Return(nil)
	mockRepository.EXPECT().Insert(ctx, company).Return(errors.New("error my bad"))

	i := &CompanyImporter{
		repository: mockRepository,
	}
	type args struct {
		record []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should successfully insert the company",
			args: args{
				record: []string{"1", "wildan"},
			},
		},
		{
			name: "should return unexpected error insert",
			args: args{
				record: []string{"1", "wildan"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing company id",
			args: args{
				record: []string{"asu", "wildan"},
			},
			wantErr: true,
		},
		{
			name: "should return error on invalid number of columns",
			args: args{
				record: []string{"asu", "wildan", "tati"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := i.Insert(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("CompanyImporter.Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
