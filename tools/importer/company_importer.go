package importer

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/wildan2711/web-orders/models"
	"gitlab.com/wildan2711/web-orders/repository/company"
)

const (
	ColCompanyID = iota
	ColCompanyName
)

type CompanyImporter struct {
	repository company.Repository
}

func (i *CompanyImporter) Insert(record []string) error {
	if len(record) != ColCompanyName+1 {
		return errors.New("invalid number of columns")
	}

	company := models.Company{
		Name: record[ColCompanyName],
	}
	companyID, err := strconv.Atoi(record[ColCompanyID])
	if err != nil {
		return fmt.Errorf("error Atoi companyID: %w", err)
	}
	company.ID = companyID

	if err := i.repository.Insert(context.Background(), &company); err != nil {
		return fmt.Errorf("error repo insert: %w", err)
	}

	return nil
}
