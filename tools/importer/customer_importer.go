package importer

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/wildan2711/web-orders/models"
	"gitlab.com/wildan2711/web-orders/repository/customer"
)

const (
	ColCustomerID = iota
	ColCustomerLogin
	ColCustomerPassword
	ColCustomerName
	ColCustomerCompanyID
	ColCustomerCreditCards
)

type CustomerImporter struct {
	repository customer.Repository
}

func (i *CustomerImporter) Insert(record []string) error {
	if len(record) != ColCustomerCreditCards+1 {
		return errors.New("invalid number of columns")
	}

	customer := models.Customer{
		ID:       record[ColCustomerID],
		Login:    record[ColCustomerLogin],
		Password: record[ColCustomerPassword],
		Name:     record[ColCustomerName],
	}
	companyID, err := strconv.Atoi(record[ColCustomerCompanyID])
	if err != nil {
		return fmt.Errorf("error Atoi companyID: %w", err)
	}
	creditCards := strings.Split(record[ColCustomerCreditCards], ",")

	customer.CompanyID = companyID
	customer.CreditCards = creditCards

	if err := i.repository.Insert(context.Background(), &customer); err != nil {
		return fmt.Errorf("error repo insert: %w", err)
	}

	return nil
}
