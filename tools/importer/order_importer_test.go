package importer

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	mock_order "gitlab.com/wildan2711/web-orders/repository/order/mocks"
)

func TestOrderImporter_Insert(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	now := time.Now()
	nowString := now.Format(time.RFC3339)

	mockRepository := mock_order.NewMockRepository(ctrl)
	mockRepository.EXPECT().Insert(ctx, gomock.Any()).Return(nil)
	mockRepository.EXPECT().Insert(ctx, gomock.Any()).Return(errors.New("error my bad"))

	i := &OrderImporter{
		repository: mockRepository,
	}
	type args struct {
		record []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should successfully insert the order",
			args: args{
				record: []string{"1", nowString, "123X", "wildan"},
			},
		},
		{
			name: "should return unexpected error on insert",
			args: args{
				record: []string{"1", nowString, "123X", "wildan"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing order id",
			args: args{
				record: []string{"babi", nowString, "123X", "wildan"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing created at",
			args: args{
				record: []string{"1", "", "123X", "wildan"},
			},
			wantErr: true,
		},
		{
			name: "should return error on invalid number of columns",
			args: args{
				record: []string{"wildan"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := i.Insert(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("DeliveryImporter.Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
