package importer

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/wildan2711/web-orders/models"
	"gitlab.com/wildan2711/web-orders/repository/order"
)

const (
	ColOrderID = iota
	ColOrderCreatedAt
	ColOrderName
	ColOrderCustomerID
)

type OrderImporter struct {
	repository order.Repository
}

func (i *OrderImporter) Insert(record []string) error {
	if len(record) != ColOrderCustomerID+1 {
		return errors.New("invalid number of columns")
	}

	order := models.Order{
		OrderName:  record[ColOrderName],
		CustomerID: record[ColOrderCustomerID],
	}
	orderID, err := strconv.Atoi(record[ColOrderID])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}
	createdAt, err := time.Parse(time.RFC3339, record[ColOrderCreatedAt])
	if err != nil {
		return fmt.Errorf("error time parse: %w", err)
	}

	order.ID = orderID
	order.CreatedAt = createdAt

	if err := i.repository.Insert(context.Background(), &order); err != nil {
		return fmt.Errorf("error repo insert: %w", err)
	}

	return nil
}
