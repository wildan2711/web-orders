package importer

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/wildan2711/web-orders/models"
	mock_customer "gitlab.com/wildan2711/web-orders/repository/customer/mocks"
)

func TestCustomerImporter_Insert(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	customer := &models.Customer{
		ID:          "wildan",
		Login:       "wildan",
		Password:    "password",
		Name:        "wildan",
		CompanyID:   1,
		CreditCards: []string{"123", "123"},
	}

	mockRepository := mock_customer.NewMockRepository(ctrl)
	mockRepository.EXPECT().Insert(ctx, customer).Return(nil)
	mockRepository.EXPECT().Insert(ctx, customer).Return(errors.New("error my bad"))

	i := &CustomerImporter{
		repository: mockRepository,
	}
	type args struct {
		record []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should successfully insert the customer",
			args: args{
				record: []string{"wildan", "wildan", "password", "wildan", "1", "123,123"},
			},
		},
		{
			name: "should return unexpected error on insert",
			args: args{
				record: []string{"wildan", "wildan", "password", "wildan", "1", "123,123"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing company id",
			args: args{
				record: []string{"wildan", "wildan", "password", "wildan", "haha", "123,123"},
			},
			wantErr: true,
		},
		{
			name: "should return error on invalid number of columns",
			args: args{
				record: []string{"wildan"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := i.Insert(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("CustomerImporter.Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
