package importer

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/wildan2711/web-orders/models"
	"gitlab.com/wildan2711/web-orders/repository/order"
)

const (
	ColOrderItemID = iota
	ColOrderItemOrderID
	ColOrderItemPrice
	ColOrderItemQuantity
	ColOrderItemProduct
)

type OrderItemImporter struct {
	repository order.Repository
}

func (i *OrderItemImporter) Insert(record []string) error {
	if len(record) != ColOrderItemProduct+1 {
		return errors.New("invalid number of columns")
	}

	orderItem := models.OrderItem{
		Product: record[ColOrderItemProduct],
	}
	orderItemID, err := strconv.Atoi(record[ColOrderItemID])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}
	orderID, err := strconv.Atoi(record[ColOrderItemOrderID])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}
	quantity, err := strconv.Atoi(record[ColOrderItemQuantity])
	if err != nil {
		return fmt.Errorf("error Atoi orderID: %w", err)
	}
	if record[ColOrderItemPrice] != "" {
		orderItem.Price, err = strconv.ParseFloat(record[ColOrderItemPrice], 64)
		if err != nil {
			return fmt.Errorf("error Atoi orderID: %w", err)
		}
	}

	orderItem.ID = orderItemID
	orderItem.OrderID = orderID
	orderItem.Quantity = quantity

	if err := i.repository.InsertItem(context.Background(), &orderItem); err != nil {
		return fmt.Errorf("error repo insert: %w", err)
	}

	return nil
}
