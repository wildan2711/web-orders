package importer

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/wildan2711/web-orders/models"
	mock_order "gitlab.com/wildan2711/web-orders/repository/order/mocks"
)

func TestOrderItemImporter_Insert(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	orderItem := &models.OrderItem{
		ID:       1,
		OrderID:  1,
		Price:    123.123,
		Quantity: 12,
		Product:  "product",
	}

	mockRepository := mock_order.NewMockRepository(ctrl)
	mockRepository.EXPECT().InsertItem(ctx, orderItem).Return(nil)
	mockRepository.EXPECT().InsertItem(ctx, orderItem).Return(errors.New("error my bad"))

	i := &OrderItemImporter{
		repository: mockRepository,
	}
	type args struct {
		record []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should successfully insert the order item",
			args: args{
				record: []string{"1", "1", "123.123", "12", "product"},
			},
		},
		{
			name: "should return unexpected error on insert",
			args: args{
				record: []string{"1", "1", "123.123", "12", "product"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing order item id",
			args: args{
				record: []string{"babi", "1", "123.123", "12", "product"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing order id",
			args: args{
				record: []string{"1", "xxx", "123.123", "12", "product"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing price",
			args: args{
				record: []string{"1", "1", "haha", "12", "product"},
			},
			wantErr: true,
		},
		{
			name: "should return error on parsing quantity",
			args: args{
				record: []string{"1", "1", "123.123", "yo", "product"},
			},
			wantErr: true,
		},
		{
			name: "should return error on invalid number of columns",
			args: args{
				record: []string{"wildan"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := i.Insert(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("DeliveryImporter.Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
