package main

import (
	"fmt"
	"os"

	"gitlab.com/wildan2711/web-orders/cmd/api"
	"gitlab.com/wildan2711/web-orders/cmd/importer"
)

func main() {
	rootCmd := api.Command()

	rootCmd.AddCommand(importer.Command())

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
