package mongodb

import (
	"context"
	"fmt"

	"gitlab.com/wildan2711/web-orders/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// FromConfig returns a mongo database connection from the config
func FromConfig(cfg *config.Config) (*mongo.Database, error) {
	clientOptions := options.Client().ApplyURI(cfg.Connection.Mongo.URI)

	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		return nil, fmt.Errorf("error connecting mongodb: %w", err)
	}

	return client.Database(cfg.Connection.Mongo.DB), nil
}
