package pgdb

import (
	"fmt"

	"github.com/go-pg/pg/v9"
	"gitlab.com/wildan2711/web-orders/config"
)

// FromConfig returns a new go-pg connection from the configuration
func FromConfig(cfg *config.Config) (*pg.DB, error) {
	opt, err := pg.ParseURL(cfg.Connection.Postgres.URI)
	if err != nil {
		return nil, fmt.Errorf("error parsing pg uri: %w", err)
	}

	db := pg.Connect(opt)

	_, err = db.Exec("SELECT version()")
	if err != nil {
		return nil, fmt.Errorf("error querying version: %w", err)
	}

	return db, nil
}
