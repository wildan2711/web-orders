package set

// Set is a data type to store unique values
type Set map[string]struct{}

// Has checks whether the value exists in the set
func (s Set) Has(value string) bool {
	_, ok := s[value]
	return ok
}

// Clear clears the set
func (s Set) Clear() {
	s = make(Set)
}

// Add adds a value to the set, maintaining uniqueness
func (s Set) Add(value string) {
	s[value] = struct{}{}
}

// Remove removes a value from the set
func (s Set) Remove(value string) {
	delete(s, value)
}

// ToSlice transforms the set into a slice
func (s Set) ToSlice() []string {
	slice := make([]string, len(s))
	idx := 0
	for value := range s {
		slice[idx] = value
		idx++
	}
	return slice
}

// NewSet returns a Set with the passed values
func NewSet(values ...string) Set {
	set := make(Set)
	for _, value := range values {
		set[value] = struct{}{}
	}
	return set
}
