package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// Load loads the configuration file from the file system
func Load() (*Config, error) {
	viper.AddConfigPath("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, fmt.Errorf("error reading config: %w", err)
	}

	currentConfig := &Config{}
	if err = viper.Unmarshal(currentConfig); err != nil {
		return nil, fmt.Errorf("error unmarshalling config: %w", err)
	}

	return currentConfig, nil
}
