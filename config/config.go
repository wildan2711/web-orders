package config

type Config struct {
	Server     Server     `mapstructure:"server"`
	Connection Connection `mapstructure:"connection"`
}

type Server struct {
	Address   string `mapstructure:"address"`
	StaticDir string `mapstructure:"static_dir"`
}

type Connection struct {
	Mongo    Mongo    `mapstructure:"mongo"`
	Postgres Postgres `mapstructure:"postgres"`
}

type Mongo struct {
	URI string `mapstructure:"uri"`
	DB  string `mapstructure:"db"`
}

type Postgres struct {
	URI   string `mapstructure:"uri"`
	Debug bool   `mapstructure:"debug"`
}
